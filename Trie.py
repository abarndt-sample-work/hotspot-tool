class TrieNode:
    def __init__(self, char):
        self.char = char
        self.children = {}
        self.isEnd = False


class Trie:

    def __init__(self, text=''):
        self.root = TrieNode('')

    def insert(self, word):
        current = self.root
        for char in word:
            if char in current.children:
                current = current.children[char]
            else:
                new_node = TrieNode(char)
                current.children[char] = new_node
                current = new_node
        current.isEnd = True

    def search(self, word):
        current = self.root
        for char in word:
            if char not in current.children:
                return False
            current = current.children[char]
        return current.isEnd
