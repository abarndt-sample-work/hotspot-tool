import ebird.api as ebird
import json
import Key
from datetime import datetime

records = ebird.get_observations(Key.api_key, 'L1000726', back=30)
records4 = ebird.get_regions(Key.api_key, 'subnational2', 'US-NY')
records2 = ebird.get_historic_observations(Key.api_key,
                                           area='US-NM-001', date=datetime(2019, 4, 27),
                                           max_results=10000, hotspot=True,)
#records2 = ebird.get_historic_observations(Key.api_key, '', "04-05-2020", 10000, 'en', False, False, 'simple', None)
records3 = ebird.get_species_observations(Key.api_key, 'horlar', 'US-NM-001', back=30, hotspot=True)
print(records4)
