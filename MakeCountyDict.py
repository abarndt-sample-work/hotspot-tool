import ebird.api as ebird
import Key

countyList = {}


def main():
    make_list()
    print(countyList)
    print(len(countyList).__str__())


def add_counties(state):
    records = ebird.get_regions(Key.api_key, 'subnational2', 'US-' + state)
    for county in records:
        countyList[county['name'] + '-' + state] = county['code']


def make_list():
    add_counties('AK')
    add_counties('AL')
    add_counties('AR')
    add_counties('AZ')
    add_counties('CA')
    add_counties('CO')
    add_counties('CT')
    add_counties('DE')
    add_counties('FL')
    add_counties('GA')
    add_counties('HI')
    add_counties('ID')
    add_counties('IL')
    add_counties('IN')
    add_counties('IA')
    add_counties('KS')
    add_counties('KY')
    add_counties('LA')
    add_counties('ME')
    add_counties('MD')
    add_counties('MA')
    add_counties('MI')
    add_counties('MN')
    add_counties('MS')
    add_counties('MO')
    add_counties('MT')
    add_counties('NE')
    add_counties('NV')
    add_counties('NH')
    add_counties('NJ')
    add_counties('NM')
    add_counties('NY')
    add_counties('NC')
    add_counties('ND')
    add_counties('OH')
    add_counties('OK')
    add_counties('OR')
    add_counties('PA')
    add_counties('RI')
    add_counties('SC')
    add_counties('SD')
    add_counties('TN')
    add_counties('TX')
    add_counties('UT')
    add_counties('VT')
    add_counties('VA')
    add_counties('WA')
    add_counties('WV')
    add_counties('WI')
    add_counties('WY')

if __name__ == '__main__':
    main()